#include <iostream>
#include <time.h>

using namespace std;

void createArray(int* numbers, int size)
{
	for (int i = 0; i < size; i++) numbers[i] = rand() % 100 + 1;
}

void printArray(int* numbers, int size)
{
	for (int i = 0; i < size; i++) cout << numbers[i] << ' ';
}

void removeElement(int* numbers, int &size, int value)
{
	int count = 0;

	for (int i = 0; i < size; i++)
	{
		if (numbers[i] == value)
		{
			for (int j = i; j < size; j++) numbers[j] = numbers[j + 1];
			size--; count++;
		}
	}
	if (count == 0) cout << "No element found" << endl;
	else cout << "Element successfully deleted" << endl;
}

int linearSearch(int* numbers, int size, int value)
{
	bool flag = false;

	for (int i = 0; i < size; i++)
	{
		if (numbers[i] == value) 
		{ return i; break; }
	}

	if (flag == false) return -1;
}

int main()
{
	srand(time(0));
	int size, value, index;

	cout << "Enter size of array: ";
	cin >> size;

	//DYNAMICALLY CREATING AN ARRAY
	int* numbers = new int[size];
	createArray(numbers, size);

	cout << "Generated array: ";
	printArray(numbers, size);

	//REMOVING ELEMENT
	cout << "\n\nEnter element to be removed: ";
	cin >> value;
	removeElement(numbers, size, value);

	cout << "Updated array: ";
	printArray(numbers, size);

	cout << "\n\nEnter element to find: ";
	cin >> value;
	index = linearSearch(numbers, size, value);

	if (index == -1) cout << value << " is not found in the array" << endl;
	else cout << value << " found on index " << index << endl;


	delete[] numbers;
	system("pause");
	return 0;
}