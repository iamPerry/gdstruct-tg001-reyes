#include <iostream>
using namespace std;

void computeFibo(int num, int &prev, int &next)
{
	int sum = prev + next;
	if (num <= 0)
		return;
	
	cout << sum << ' ';
	prev = next;
	next = sum;
	computeFibo(num - 1, prev, next);
}

void main()
{
	int num, prev = 0, next = 1;

	cout << "Input number: ";
	cin >> num;

	if (num == 1) cout << 1 << endl;
	else {
		cout << next << ' ';
		computeFibo(num-1, prev, next);
	}

	system("pause");
}