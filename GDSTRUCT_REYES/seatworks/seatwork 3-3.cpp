#include <iostream>  
using namespace std;

void checkIfPrime(int num, int count)
{
	if (count == num) {
		cout << "Number is a prime" << endl;
		return;
	}

	if (num % count == 0) {

		cout << "Number is not a prime" << endl;
		return;
	}
	checkIfPrime(num, count + 1);
		
}

int main()
{
	int num;

	cout << "Enter the Number to check Prime: ";
	cin >> num;
	
	if (num == 1) cout << "Number is a prime" << endl;
	else	checkIfPrime(num, 2);

	system("pause");
	return 0;
}