#include <iostream>
using namespace std;

void computeSum(int &num, int &sum)
{
	if (num <= 0)
	{
		cout << "Sum: " << sum << endl;
		return;
	}
		
	int modulo = num % 10;
	num = num / 10;

	sum = sum + modulo;
	computeSum(num, sum);
}

void main() 
{
	int num, sum = 0;

	cout << "Input num: ";
	cin >> num;
	computeSum(num, sum);

	system("pause");
}