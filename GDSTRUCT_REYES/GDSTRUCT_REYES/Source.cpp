#include <iostream>
#include <time.h>

using namespace std;

void randomize(int integers[]) {
	cout << "GENERATED ARRAY:\t";
	for (int i = 0; i<10; i++) {
		integers[i] = rand() % 69 + 1;
		cout << integers[i] << ' ';
	}
	cout << endl;
}

void ascending(int integers[]) {
	int temp;

	cout << "ASCENDING ARRAY:\t";
	for (int x = 0; x<10; x++) {
		for (int y = x + 1; y<10; y++) {
			if (integers[x] > integers[y]) {
				temp = integers[x];
				integers[x] = integers[y];
				integers[y] = temp;
			}
		}
		cout << integers[x] << ' ';
	}
	cout << endl;
}

void descending(int integers[]) {
	int temp;

	cout << "DESCENDING ARRAY:\t";
	for (int x = 0; x<10; x++) {
		for (int y = x + 1; y<10; y++) {
			if (integers[x] < integers[y]) {
				temp = integers[x];
				integers[x] = integers[y];
				integers[y] = temp;
			}
		}
		cout << integers[x] << ' ';
	}
	cout << endl;
}

void search(int integers[]) {
	bool found = false;
	int number;

	cout << "What number are you looking for?: ";
	cin >> number;
	for (int i = 0; i < 10; i++) {
		if (number == integers[i]) {
			cout << "Number " << number << " found on index " << i << endl;
			found = true;
		}
	}
	if (found == false) {
		cout << "Number " << number << " was not found on the array. Try again." << endl;
	}
}

int main() {
	srand(time(NULL));
	int numbers[10];
	char sort;

	//WILL GENERATE A RANDOMIZED ARRAY
	randomize(numbers);

	//WILL PROMPT USER HOW THE ARRAY WILL BE SORTED
	cout << "ARRANGE THE ARRAY FROM (A)ASCENDING OR (D)DESCENDING?\n";

	//HANDLER FOR INPUT
	do {
		cout << "Please key in 'a' or 'd' only: ";
		cin >> sort;
	} while ((sort != 'a') && (sort != 'd'));

	system("cls");

	//WILL CALL FUNCTION ASCENDING OR DESCENDING
	if (sort == 'a') ascending(numbers);
	else if (sort == 'd') descending(numbers);

	//WILL LET THE USER FOR A NUMBER IN THE ARRAY
	search(numbers);

	system("pause");
	return 0;
}