#include <iostream>
#include <string>
#include "UnorderedArray.h"
#include "OrderedArray.h"
#include <time.h>
#include <conio.h>

using namespace std;

int addSize() 
{
	cout << "Enter size of array: ";
	int size;
	cin >> size;

	return size;
}

void pushElements(UnorderedArray<int>& unordered, OrderedArray<int>& ordered, int size)
{
	for (int i = 0; i < size; i++)
	{
		int rng = rand() % 101;
		unordered.push(rng);
		ordered.push(rng, i);
	}
}

void printArray(UnorderedArray<int> unordered, OrderedArray<int> ordered)
{
	cout << "GENERATED ARRAY\n" << endl;
	cout << "Unordered:\t";
	for (int i = 0; i < unordered.getSize(); i++)
		cout << unordered[i] << "   ";
	
	cout << "\nOrdered:\t";
	for (int i = 0; i < ordered.getSize(); i++)
		cout << ordered[i] << "   ";
	
	cout << "\n" << endl;
}

void removeElement(UnorderedArray<int> unordered, OrderedArray<int> ordered)
{
	int index, num;
	cout << "Enter index to remove: ";
	cin >> num;

	index = num;

	system("cls");

	cout << "Removed element " << unordered[index] << " from Unordered array";
	cout << "\nRemoved element " << ordered[index] << " from Ordered array" << endl;
	
	unordered.remove(index);
	ordered.remove(index);

	system("pause");
}

void searchElement(UnorderedArray<int> unordered, OrderedArray<int> ordered)
{
	int number, lIndex, bIndex;

	cout << "Input Element to search: ";
	cin >> number;

	//linear search in Unordered array
	lIndex = unordered.linearSearch(number);
	cout << "Linear search took " << lIndex + 1 << " comparison." << endl;
	
	//binary search in Ordered array
	int count = 0;
	bIndex = ordered.binarySearch(number, 0, (ordered.getSize()-1), count);
	cout << "Binary search took " << count << " comparison." << endl;

	//this will print the index number of the element
	cout << "\nElement " << number << " is found on each index" << endl;
	cout << "Unordered Array index " << lIndex << endl;
	cout << "Ordered Array index " << bIndex << endl;
}

void expandArray(UnorderedArray<int>& unordered, OrderedArray<int>& ordered)
{
	int add;
	cout << "Input size of expansion: ";
	cin >> add;

	pushElements(unordered, ordered, add);
	cout << "Arrays has been expanded" << endl;

	printArray(unordered, ordered);
}

void main()
{
	srand(time(0));

	//prompts the user to enter size of array
	int size = addSize();
	char ch;

	UnorderedArray<int> unordered(size);
	OrderedArray<int> ordered(size);

	//push elements to unordered and ordered array
	pushElements(unordered, ordered, size);

	system("pause");
	
	do {
		system("cls");
		printArray(unordered, ordered);

		cout << "a. Remove Element\n"
			<< "b. Search Element\n"
			<< "c. Expand Array\n\n"
			<< "x. Quit" << endl;
		ch = _getch();

		if (ch == 'a') removeElement(unordered, ordered);
		if (ch == 'b') searchElement(unordered, ordered);
		if (ch == 'c') expandArray(unordered, ordered);
	} while (ch != 'x');

	system("pause");
}